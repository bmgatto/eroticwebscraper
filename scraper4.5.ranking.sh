#!/bin/bash

rm ./wordsthatarenaughty.txt

for i in $(cat ./naughtywords.txt); do
    echo $i
    grep -ioP "[ >]{1}$i[ .,]{1}" ./longstory.txt | grep -Po "[A-z]*" >> ./wordsthatarenaughty.txt
done
