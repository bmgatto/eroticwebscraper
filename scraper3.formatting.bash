#!/bin/bash

# Requirements: urls.txt
#     The file with all the urls in it
# Output: longstory.txt
#     A fully formatted file with all the stories in series

# Goes through every page of every story, downloads it, and formats it before
#  outputting it to the longstory.txt file

# For each url from all categories
for i in $(less ./urls.txt); do

    # This just puts a separator between the stories
    cat storysep.txt >> longstory.txt
    
    # This checks the bottom of the page to see how many pages there are, then
    #  stores the highest number
    # We use this later to make sure we have all the pages too
    numofpages=$(curl "$i" \
        | grep -Po '>[0-9]+</a><form action="?"' \
        | cut -d'<' -f1 \
        | cut -d'>' -f2)

    # As much as I would love to use just the for loop, for some reason the if
    #  you put page=1 in the url it breaks the page
    # This block is just to get the first page and format it
    curl "$i" | tail -n1 | grep -o '{"pageText":"[^}]*}' \
        | sed 's/\\\\r\\\\n/\n/g' \
        | sed 's/\\\\//g' \
        | sed 's/{"pageText":"//' \
        | sed 's/r"}//' \
        | sed 's/\}//g' \
        | sed "s/\\\'/'/g" >> longstory.txt

    # Now this loop uses that $numofpages variable we created earlier to
    #  iterate through each page of the story and grab the story contents
    # Works exactly like the last one, but just more of it
    for j in {2..$numofpages}
    do
        # Grabs the pages and formats them, selecting out the story text 
        #  and then basically just removing a bunch of backslashes
        curl "$i?page=$j" | tail -n1 | grep -o '{"pageText":"[^}]*}' \
            | sed 's/\\\\r\\\\n/\n/g' \
            | sed 's/\\\\//g' \
            | sed 's/{"pageText":"//' \
            | sed 's/\}//g' \
            | sed 's/r"}//' | sed "s/\\\'/'/g" >> longstory.txt
    done

done
