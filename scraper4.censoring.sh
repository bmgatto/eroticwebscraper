#!/bin/bash

cp ./longstory.txt cleanstory.txt

for i in $(cat ./naughtywords.txt); do
    if $(grep -qo $i cleanstory.txt); then
        echo $i
        sed -i "s/\([^A-z]\)$i\([^A-z]\)/\1$(shuf -n1 ./cleanwords.txt)\2/g" cleanstory.txt
    fi
done

