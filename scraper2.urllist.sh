#!/bin/bash

# Requirements: categories.txt
#     Contains all of the urls to the individual categories
# Output: urls.txt
#     Contains all of the links to the individual stories from each category

# TODO Redo the script that grabs the categories
# TODO Include functionality to grab the highest rated and most read categories

# For each entry in the categories.txt file
for i in $(cat ./categories.txt); do

    # For each page from 1 to 5
    for j in {1..5}; do
        # Grab every link on each category page
        curl $i'/alltime/?page='$j \
            | grep -Po 'https://www.literotica.com/s/[A-z0-9-]*' >> urls.txt
    done

done
